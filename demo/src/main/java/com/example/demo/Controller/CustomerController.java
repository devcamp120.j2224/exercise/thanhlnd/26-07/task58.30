package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CustomerList;
import com.example.demo.Respository.iCustomerList;

@RestController
@CrossOrigin
public class CustomerController {
    
    @Autowired
    iCustomerList customerList ;

    @GetMapping("customerList")
    public ResponseEntity <List <CustomerList>> getCustomerList() {
        
        try {
            List<CustomerList> listCusomter = new ArrayList<CustomerList>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            customerList.findAll().forEach(listCusomter :: add);

            if(listCusomter.size() == 0){
                return new ResponseEntity<List <CustomerList>>(listCusomter, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <CustomerList>>(listCusomter, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
